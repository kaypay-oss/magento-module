define([
    'Magento_Checkout/js/view/payment/default'
], function (
    Component
) {
    'use strict';

    var config = window.checkoutConfig.payment;

    return Component.extend({
        defaults: {
            template: 'Kaypay_Payment/payment/kaypay_method'
        },

        redirectAfterPlaceOrder: false,
        afterPlaceOrder: function () {
            window.location.replace(config[this.getCode()].checkoutUrl);
        }
    });
});
