define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';

        rendererList.push(
            {
                type: 'kaypay',
                component: 'Kaypay_Payment/js/view/payment/method-renderer/kaypay-method'
            }
        );
        return Component.extend({});
    }
);
