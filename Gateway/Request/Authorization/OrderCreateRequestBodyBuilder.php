<?php

namespace Kaypay\Payment\Gateway\Request\Authorization;

use Kaypay\Sdk\Model\OrderCreateRequestBody;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\UrlInterface;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Sales\Model\Order\Payment;
use Magento\Store\Model\StoreManagerInterface;

class OrderCreateRequestBodyBuilder implements BuilderInterface
{
    const REF_INCREMENT_ID = 'incrementId';

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    public function __construct(
        SerializerInterface   $serializer,
        StoreManagerInterface $storeManager,
        UrlInterface          $urlBuilder
    )
    {
        $this->serializer = $serializer;
        $this->storeManager = $storeManager;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * @inheritdoc
     *
     * @throws NoSuchEntityException
     * @see \Magento\Payment\Gateway\Data\PaymentDataObjectFactory::create
     * @see \Magento\Payment\Model\Method\Adapter::executeCommand
     */
    public function build(array $buildSubject)
    {
        if (!isset($buildSubject['payment'])
            || !$buildSubject['payment'] instanceof PaymentDataObjectInterface
        ) {
            throw new \InvalidArgumentException('Payment data object should be provided');
        }

        $paymentDataObject = $buildSubject['payment'];
        $order = $paymentDataObject->getOrder();
        $incrementId = $order->getOrderIncrementId();
        $storeName = $this->storeManager->getStore()->getName();
        $orderCreateRequestBody = (new OrderCreateRequestBody())
            ->setCallbackUrl($this->urlBuilder->getUrl('kaypay/payment/callback'))
            ->setCurrency($order->getCurrencyCode())
            ->setDescription("$storeName: #$incrementId")
            ->setMerchantDisplayId($incrementId)
            ->setMerchantRefId($this->serializer->serialize([
                self::REF_INCREMENT_ID => $order->getOrderIncrementId(),
            ]));

        // we are `ceil()`ing the amount here because only VND is supported (for now)
        $orderCreateRequestBody->setTotalAmount(ceil($buildSubject['amount']));
        $payment = $paymentDataObject->getPayment();
        if ($payment instanceof Payment) {
            $orderCreateRequestBody->setShippingFee(ceil($payment->getShippingAmount()));
        }

        return [
            'OrderCreateRequestBody' => $orderCreateRequestBody,
        ];
    }
}
