<?php

namespace Kaypay\Payment\Gateway\Response;

use Kaypay\Payment\Model\Ui\ConfigProvider;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Gateway\Response\HandlerInterface;
use Magento\Sales\Model\Order\Payment;

class TransactionIdHandler implements HandlerInterface
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @param SerializerInterface $serializer
     */
    public function __construct(
        SerializerInterface $serializer
    )
    {
        $this->serializer = $serializer;
    }

    public function handle(array $handlingSubject, array $response)
    {
        if (!isset($handlingSubject['payment'])
            || !$handlingSubject['payment'] instanceof PaymentDataObjectInterface
        ) {
            throw new \InvalidArgumentException('Payment data object should be provided');
        }

        $orderId = $response['orderId'];
        $redirectUrl = $response['redirectUrl'];
        if (empty($orderId) || empty($redirectUrl)) {
            throw new \InvalidArgumentException("Response doesn't contain expected data");
        }

        $paymentDataObject = $handlingSubject['payment'];
        $payment = $paymentDataObject->getPayment();
        if ($payment instanceof Payment) {
            $payment->setIsTransactionClosed(false);
            $payment->setIsTransactionPending(true);
            $payment->setTransactionId($orderId);
            $payment->setTransactionAdditionalInfo(ConfigProvider::TXN_REDIRECT_URL, $redirectUrl);
            $payment->setTransactionAdditionalInfo(ConfigProvider::TXN_RAW_DETAILS, $this->serializer->serialize($response));
        }
    }
}
