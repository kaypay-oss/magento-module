<?php

namespace Kaypay\Payment\Gateway\Http\Client;

use GuzzleHttp\Exception\GuzzleException;
use Kaypay\Payment\Model\Ui\ConfigProvider;
use Kaypay\Sdk\Api\OrderApi;
use Kaypay\Sdk\ApiException;
use Kaypay\Sdk\Configuration;
use Kaypay\Sdk\Model\OrderCreateRequestBody;
use Kaypay\Sdk\Model\OrderCreateSuccess;
use Kaypay\Sdk\SandboxConfiguration;
use Kaypay\Sdk\Signer;
use Magento\Payment\Gateway\Config\Config;
use Magento\Payment\Gateway\Http\ClientInterface;
use Magento\Payment\Gateway\Http\TransferInterface;
use Magento\Payment\Model\Method\Logger;

class KaypaySdk implements ClientInterface
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var Logger
     */
    private $logger;

    public function __construct(
        Config $config,
        Logger $logger
    )
    {
        $this->config = $config;
        $this->logger = $logger;
    }

    /**
     * @throws GuzzleException
     * @throws ApiException
     */
    public function placeRequest(TransferInterface $transferObject)
    {
        /** @var Configuration $configuration */
        $configuration = null;
        if ($this->config->getValue(ConfigProvider::CONFIG_SANDBOX) > 0) {
            $configuration = new SandboxConfiguration();
        }
        $signer = new Signer($this->config->getValue(ConfigProvider::CONFIG_SECRET_KEY));
        $api = new OrderApi($signer, null, $configuration);

        $transferBody = $transferObject->getBody();
        /** @var OrderCreateRequestBody $orderCreateRequestBody */
        $orderCreateRequestBody = $transferBody['OrderCreateRequestBody'];
        $orderCreateRequestBody->setMerchantCode($this->config->getValue(ConfigProvider::CONFIG_MERCHANT_CODE));

        /** @var OrderCreateSuccess $success */
        $success = $api->postV1Orders($orderCreateRequestBody);
        $successData = (array)$success->getData()->jsonSerialize();

        $this->logger->debug([
            'orderCreateRequestBody' => $orderCreateRequestBody->jsonSerialize(),
            'successData' => $successData
        ]);

        return $successData;
    }
}
