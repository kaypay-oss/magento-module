# Kaypay for Magento 2 

[Kaypay](https://kaypay.vn) Payment Gateway for Magento 2.

## Installation

The recommended way to install Kaypay Module for Magento 2 is through Composer:

```sh
composer require kaypay/magento2
```

Register the extension:

```sh
bin/magento setup:upgrade
```

Recompile your Magento project:

```sh
bin/magento setup:di:compile
```

Clean the cache:

```sh
bin/magento cache:clean
```
