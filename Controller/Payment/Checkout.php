<?php

namespace Kaypay\Payment\Controller\Payment;

use Kaypay\Payment\Model\Ui\ConfigProvider;
use Magento\Checkout\Model\Session;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Payment;
use Magento\Sales\Model\Order\Payment\Transaction;
use Magento\Sales\Model\Order\Payment\Transaction\Repository;

class Checkout extends Action
{
    /**
     * @var Session
     */
    private $checkoutSession;

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var Repository
     */
    private $transactionRepository;

    /**
     * @param Context $context
     * @param Session $checkoutSession
     * @param FilterBuilder $filterBuilder
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param Repository $transactionRepository
     */
    public function __construct(
        Context               $context,
        Session               $checkoutSession,
        FilterBuilder         $filterBuilder,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        Repository            $transactionRepository
    )
    {
        parent::__construct($context);
        $this->checkoutSession = $checkoutSession;
        $this->filterBuilder = $filterBuilder;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->transactionRepository = $transactionRepository;
    }

    public function execute()
    {
        $order = $this->checkoutSession->getLastRealOrder();
        $redirectUrl = $this->getRedirectUrl($order);
        if ($redirectUrl === null) {
            return $this->_redirect('/');
        }

        /** @var Redirect $redirect */
        $redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $redirect->setUrl($redirectUrl);
        return $redirect;
    }

    /**
     * @param Order $order
     * @return string|null
     */
    private function getRedirectUrl(Order $order)
    {
        $payment = $order->getPayment();
        if (!($payment instanceof Payment)) {
            return null;
        }

        $filters[] = $this->filterBuilder->setField('payment_id')
            ->setValue($payment->getId())
            ->create();
        $filters[] = $this->filterBuilder->setField('order_id')
            ->setValue($order->getId())
            ->create();
        $searchCriteria = $this->searchCriteriaBuilder->addFilters($filters)
            ->create();
        $transactionList = $this->transactionRepository->getList($searchCriteria);
        if (count($transactionList) !== 1) {
            return null;
        }

        /** @var Transaction $transaction */
        $transaction = $transactionList->getFirstItem();
        if (!($transaction instanceof Transaction)) {
            return null;
        }

        $redirectUrl = $transaction->getAdditionalInformation(ConfigProvider::TXN_REDIRECT_URL);
        if (is_string($redirectUrl)) {
            return $redirectUrl;
        } else {
            return null;
        }
    }
}
