<?php

namespace Kaypay\Payment\Controller\Payment;

use Exception;
use Kaypay\Payment\Gateway\Request\Authorization\OrderCreateRequestBodyBuilder;
use Kaypay\Payment\Model\Ui\ConfigProvider;
use Kaypay\Sdk\Model\WebhookOrderPayload;
use Kaypay\Sdk\ObjectSerializer;
use Kaypay\Sdk\Signer;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\AbstractResult;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Payment\Gateway\Config\Config;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Payment;
use Magento\Sales\Model\OrderFactory;

class Callback extends Action implements CsrfAwareActionInterface
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var OrderFactory
     */
    private $orderFactory;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @param Context $context
     * @param Config $config
     * @param OrderFactory $orderFactory
     * @param SerializerInterface $serializer
     */
    public function __construct(
        Context             $context,
        Config              $config,
        OrderFactory        $orderFactory,
        SerializerInterface $serializer
    )
    {
        parent::__construct($context);
        $this->config = $config;
        $this->orderFactory = $orderFactory;
        $this->serializer = $serializer;
    }

    /**
     * @inheritdoc
     */
    public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return true;
    }

    /**
     * @inheritdoc
     *
     * @throws Exception
     */
    public function execute()
    {
        $request = $this->getRequest();
        if (!($request instanceof Http)) {
            throw new \InvalidArgumentException('Http request is expected');
        }

        $data = $request->getContent();
        $signature = $request->getHeader(Signer::SIGNATURE_HEADER);
        $signer = new Signer($this->config->getValue(ConfigProvider::CONFIG_SECRET_KEY));
        if (!$signer->verifySignature($data, $signature)) {
            /** @var Json $unauthorizedJson */
            $unauthorizedJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $unauthorizedJson->setHttpResponseCode(401);
            $unauthorizedJson->setData(['message' => 'Invalid signature']);
            return $unauthorizedJson;
        }

        /** @var WebhookOrderPayload $payload */
        $payload = ObjectSerializer::deserialize($data, '\Kaypay\Sdk\Model\WebhookOrderPayload', []);
        switch ($payload->getType()) {
            case WebhookOrderPayload::TYPE_PAYMENT_FAILED:
                return $this->onFailed($payload);
            case WebhookOrderPayload::TYPE_PAYMENT_SUCCEEDED:
                return $this->onSucceeded($payload);
        }

        /** @var Json $noopJson */
        $noopJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $noopJson->setData(['action' => 'noop']);
        return $noopJson;
    }

    /**
     * @param WebhookOrderPayload $payload
     * @return Order
     */
    private function loadOrderByIncrementId(WebhookOrderPayload $payload)
    {
        $data = $payload->getData();
        $unserialized = $this->serializer->unserialize($data->getMerchantRefId());
        if (empty($unserialized[OrderCreateRequestBodyBuilder::REF_INCREMENT_ID])) {
            throw new \InvalidArgumentException('Unrecognized merchantRefId');
        }

        $incrementId = $unserialized[OrderCreateRequestBodyBuilder::REF_INCREMENT_ID];
        $orderModel = $this->orderFactory->create();
        return $orderModel->loadByIncrementId($incrementId);
    }

    /**
     * @param WebhookOrderPayload $payload
     * @return AbstractResult
     * @throws Exception
     */
    private function onFailed(WebhookOrderPayload $payload)
    {
        $order = $this->loadOrderByIncrementId($payload);
        $payment = $order->getPayment();
        if (!($payment instanceof Payment)) {
            throw new \InvalidArgumentException('Order payment instance is expected');
        }

        $note = $payload->getNote();
        if (!empty($note)) {
            $payment->setData('message', $note);
        }

        $voided = $payment->registerVoidNotification();
        $order->setState(Order::STATE_CANCELED);
        $order->save();

        /** @var Json $voidJson */
        $voidJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $voidJson->setData([
            'action' => 'void',
            'transactionId' => $voided->getTransactionId(),
        ]);
        return $voidJson;
    }

    /**
     * @param WebhookOrderPayload $payload
     * @return AbstractResult
     * @throws Exception
     */
    private function onSucceeded(WebhookOrderPayload $payload)
    {
        $order = $this->loadOrderByIncrementId($payload);
        $payment = $order->getPayment();
        if (!($payment instanceof Payment)) {
            throw new \InvalidArgumentException('Order payment instance is expected');
        }

        $amount = $payload->getData()->getTotalAmount();
        $captured = $payment->registerCaptureNotification($amount, true);
        $order->setState(Order::STATE_PROCESSING);
        $order->save();

        /** @var Json $captureJson */
        $captureJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $captureJson->setData([
            'action' => 'capture',
            'transactionId' => $captured->getTransactionId(),
        ]);
        return $captureJson;
    }
}
