<?php

namespace Kaypay\Payment\Model\Ui;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\UrlInterface;

final class ConfigProvider implements ConfigProviderInterface
{
    const CODE = 'kaypay';

    const CONFIG_MERCHANT_CODE = 'merchant_code';

    const CONFIG_SANDBOX = 'sandbox';

    const CONFIG_SECRET_KEY = 'secret_key';

    const CURRENCY_CODE_VND = 'VND';

    const TXN_RAW_DETAILS = 'kaypay:rawDetails';

    const TXN_REDIRECT_URL = 'kaypay:redirectUrl';

    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    public function __construct(
        UrlInterface $urlBuilder
    )
    {
        $this->urlBuilder = $urlBuilder;
    }

    public function getConfig()
    {
        return [
            'payment' => [
                self::CODE => [
                    'checkoutUrl' => $this->urlBuilder->getUrl('kaypay/payment/checkout')
                ]
            ]
        ];
    }
}
