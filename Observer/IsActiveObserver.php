<?php

namespace Kaypay\Payment\Observer;

use Kaypay\Payment\Model\Ui\ConfigProvider;
use Magento\Framework\DataObject;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Payment\Gateway\Config\Config;
use Magento\Payment\Model\Method\Adapter;
use Magento\Quote\Api\Data\CartInterface;

class IsActiveObserver implements ObserverInterface
{
    /**
     * @var Config
     */
    private $config;

    public function __construct(
        Config $config
    )
    {
        $this->config = $config;
    }

    /**
     * Disable our payment method if it is not fully configured.
     *
     * @inheritdoc
     *
     * @see \Magento\Payment\Model\Method\Adapter::isAvailable
     */
    public function execute(Observer $observer)
    {
        $event = $observer->getEvent();
        $methodInstance = $event->getData('method_instance');
        if ($methodInstance instanceof Adapter) {
            if ($methodInstance->getCode() !== ConfigProvider::CODE) {
                return;
            }
        } else {
            return;
        }

        $currencyCode = null;
        /** @var CartInterface $quote */
        $quote = $event->getData('quote');
        $quoteCurrency = $quote->getCurrency();
        if ($quoteCurrency) {
            $currencyCode = $quoteCurrency->getQuoteCurrencyCode();
        }

        $merchantCode = $this->config->getValue(ConfigProvider::CONFIG_MERCHANT_CODE);
        $secretKey = $this->config->getValue(ConfigProvider::CONFIG_SECRET_KEY);
        if ($currencyCode !== ConfigProvider::CURRENCY_CODE_VND || empty($merchantCode) || empty($secretKey)) {
            /** @var DataObject $result */
            $result = $event->getData('result');
            $result->setData('is_available', false);
        }
    }
}
